package teste.util;

//import org.junit.runners.Suite.SuiteClasses;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.openqa.selenium.server.RemoteControlConfiguration;
import org.openqa.selenium.server.SeleniumServer;
import org.testng.ITestContext;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.HttpCommandProcessor;
//import org.testng.annotations.AfterMethod;

public class BaseTest {
	
	static public SeleniumServer server;
	static public HttpCommandProcessor proc;
	static protected DefaultSelenium selenium;
	
	
	@BeforeSuite(alwaysRun=true)
	public void setupBeforeSuite(ITestContext context) throws IOException {
		Properties prop = new Properties();
		InputStream propertiesStream = getClass().getResourceAsStream("/teste.properties");
		prop.load(propertiesStream);
		
		String seleniumHost = prop.getProperty("selenium.host");
		String seleniumPort = prop.getProperty("selenium.port");
		String seleniumBrowser = prop.getProperty("selenium.browser");
		String seleniumUrl = "";

	    	
		seleniumUrl = prop.getProperty("selenium.url.in");
	  
		RemoteControlConfiguration rcc = new RemoteControlConfiguration();
		rcc.setSingleWindow(true);
		rcc.setPort(Integer.parseInt(seleniumPort));
	  	
		try {
			server = new SeleniumServer(false, rcc);
			server.boot();
		} catch (Exception e) {
			throw new IllegalStateException("Can't start selenium server", e);
		}
	  
		proc = new HttpCommandProcessor(seleniumHost, Integer.parseInt(seleniumPort), seleniumBrowser, seleniumUrl);
		selenium = new DefaultSelenium(proc);
		selenium.start();
	}
	
	
//	@AfterMethod
	public void fecharModal(){
		selenium.click("css=img[alt=\"Fechar\"]");
	}

	
	//@AfterSuite(alwaysRun=false)
	public void setupAfterSuite() {
		selenium.stop();
		server.stop();
	
	}
	
}
