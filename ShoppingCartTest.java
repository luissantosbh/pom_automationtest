
package teste.util;

import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Pages.CartPage;
import Pages.CheckoutPage;
import Pages.HomePage;
import Pages.ProductPage;

@Test


public class ShoppingCartTest extends BaseTest{

	private WebDriver driver;

	  
	 @BeforeClass
	 public void beforeClass() {
	 driver=new FirefoxDriver();
	 driver.navigate().to("http://www.williams-sonoma.com");
	 driver.manage().window().maximize();
	 }	
	
	public void shoppingCart(){
			
			
		
		 
		ProductPage p = new ProductPage(); 
				p.setNmProduct("OXO Adjustable Potato Ricer");
				p.getNmCategory().setNmCategory("Food Mills");
				
				//setNmCategory("Food Mills");
						
		selenium.open("/");
    	speedTest("slow");
    	
    	HomePage h = new HomePage();
    	h.setHomeCategory(p.getNmCategory());
    	
       //HomePage h = new HomePage();  	
       //h.setHomeCategoria(p.getNmCategory()); 
		
    	
    	//Flyout menu
    	selenium.click("link="+h.getHomeCategory().getNmCategory());
		selenium.waitForPageToLoad("30000");
						
		//Choosing a product
				
	    selenium.click("xpath=/html/body/div[1]/div[2]/div[3]/ul/li[2]/a/span/text()['"+ p.getNmProduct() +"']");
    	selenium.waitForPageToLoad("30000");
    	
	
    	//TC 1 - Verify if Product page shows "Add to cart button"
   	    	
    	assertTrue(selenium.isVisible("xpath=/html/body/div[1]/div[1]/div[7]/div[2]/div[2]/section/div/div/fieldset[1]/button"));

	    	    	
		//Adding a product to cart
		assertTrue(selenium.isTextPresent(p.getNmProduct()));
		
		CartPage cp=PageFactory.initElements(driver, CartPage.class);
		cp.ClickaddToCart();
		
		
		//TC 2 - Verify if add to cart overlay appears
		assertTrue(selenium.isTextPresent("You've just added the following to your basket:"));

		//TC 3 - Verify if Checkout button is on the add to cart overlay
		assertTrue(selenium.isVisible("id=anchor-btn-checkout"));
		
		//TC 4 Verify if the Shopping cart page is shown, after clicks on Checkout's button
		//Checkout
		CheckoutPage ckp = PageFactory.initElements(driver, CheckoutPage.class);
		ckp.ClickcheckoutButton();
		
		selenium.waitForPageToLoad("30000");

		assertTrue(selenium.isElementPresent("id=shopping-cart"));	
		
		//TC 5 Verify if the product added to cart is on shopping cart page
		assertTrue(selenium.isTextPresent(p.getNmProduct()));
		
		speedTest("normal");
		
	
	}
	

	
		public static void speedTest(String velocidade){

		if (velocidade.equals("slowx2")){
			selenium.setSpeed("10000");
		}else if (velocidade.equals("slow")){
				  selenium.setSpeed("5000");
			  }else if (velocidade.equals("fast")){
				    	selenium.setSpeed("100");
					}else {
							selenium.setSpeed("3000");
						  }
	}


		@AfterClass
		  public void afterClass() {
		//Close the browser
		driver.close();
		driver.quit();
		  }
}





