package Pages;

public class ProductPage {

	String nmProduct;
    CategoryPage nmCategory = new CategoryPage();
	
    public String getNmProduct() {
		return nmProduct;
	}
	public void setNmProduct(String nmProduct) {
		this.nmProduct = nmProduct;
	}
	public CategoryPage getNmCategory() {
		return nmCategory;
	}
	public void setNmCategory(CategoryPage nmCategory) {
		this.nmCategory = nmCategory;
	}
    
}
