
package teste.util;

import static org.testng.AssertJUnit.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;




import Pages.ProductPage;
import Pages.SearchPage;

@Test
public class QuickLookTest extends BaseTest{
	
	private WebDriver driver;
	  
	 @BeforeClass
	 public void beforeClass() {
	 driver=new FirefoxDriver();
	 driver.navigate().to("http://www.williams-sonoma.com");
	 driver.manage().window().maximize();
	 }	
	
	
	
	public void quickLook(){
   
	    		   
		// Create a new instance of a driver
		WebDriver driver = new HtmlUnitDriver();
		
		// Create a new instance of the search page class
		// and initialize any WebElement fields in it.
		SearchPage page = PageFactory.initElements(driver, SearchPage.class);

		// And now do the search.
    	ProductPage p = new ProductPage(); 
		page.searchFor("['"+ p.getNmProduct() +"']");
					  
	
	   //TC 1 - Verify if search field takes to the results page
		assertTrue(selenium.isTextPresent("Search Results for:"));
		
		//Catch the price displayed at results page
		String preco = selenium.getText("xpath=/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li/span/span[2]/span[2]/span[2]/text()");

		//Quick Look
		//TC 2 - Confirm if the quick look button is displayed
		assertTrue(selenium.isElementPresent("xpath=/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li/div[1]/a[2]"));
		selenium.click("xpath=/html/body/div[1]/div/div[2]/div[2]/div[2]/ul/li/div[1]/a[2]");	
		
		// TC 3 - Verify if the product overlay is displayed after clicks on quick look button.
		assertTrue(selenium.isElementPresent("id=quicklookOverlay"));
		
		// TC 4 The product clicked should have the same name and price as the product in the overlay
		assertTrue(selenium.isTextPresent(preco));
		

		speedTest("slow");
		
	}
	
	
	public static void speedTest(String velocidade){

		if (velocidade.equals("slowx2")){
			selenium.setSpeed("10000");
		}else if (velocidade.equals("slow")){
				  selenium.setSpeed("5000");
			  }else if (velocidade.equals("fast")){
				    	selenium.setSpeed("100");
					}else {
							selenium.setSpeed("3000");
						  }
	}
	


	@AfterClass
	  public void afterClass() {
	//Close the browser
	driver.close();
	driver.quit();
	  }
}





