package Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class SearchPage {
	
    @FindBy(how = How.ID, using = "btnSearch")
    private WebElement searchBox;

    public void searchFor(String text) {
       
        searchBox.sendKeys(text);
        searchBox.click();  
        
    }

	public WebElement getSearchBox() {
		return searchBox;
	}

	public void setSearchBox(WebElement searchBox) {
		this.searchBox = searchBox;
	}
	} 
	