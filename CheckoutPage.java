package Pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class CheckoutPage {

	//Define Checkout Button
	@FindBy(how=How.ID, using="id=anchor-btn-checkout")
	private WebElement checkOutButton;
	
	public void ClickcheckoutButton()
	{
		checkOutButton.click();
	}
	

	public WebElement getCheckOutButton() {
		return checkOutButton;
	}

	public void setCheckOutButton(WebElement checkOutButton) {
		this.checkOutButton = checkOutButton;
	}
	
	
}
